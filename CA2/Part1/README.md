Gradle Basic Demo
===================

- The basic_demo application was downloaded and added to the repository 

- Used command ```./gradlew build``` to verify that the project was built correctly  

- Used command ```java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001``` to run the ChatServerApp class from the project and get the server running.

- Executed the task runClient using the command ``./gradlew runClient`` on a separate bash console

- Verified that everything was running smoothly. 

- Created the following task on the build.gradle to run the server through Gradle, automatically on port 59001:

```
task runServer(type: JavaExec, dependsOn: classes) {

    group = "DevOps"
    description = "Executes the server on localhost: 59001"

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
}
```

- Tested the task using the command ```gradle runServer```. The server ran successfully:

![](https://i.imgur.com/EiJbE4S.png)


- Attempted the command ``gradle runClient`` again on a new console. The client opened as expected, since both tasks accept the same port number as arguments. 

- Added the following dependencies on the build.gradle file's dependencies section, to be able to run unit tests: 
```
 implementation 'org.junit.jupiter:junit-jupiter:5.7.0'
 testImplementation 'org.junit.jupiter:junit-jupiter-api:5.3.1'
 testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.3.1'
```


- Added the provided AppTest.java class to the project 

- The command ``./gradlew build`` executed all tasks successfully 

- Ran the command ```gradle clean test --scan``` to run the unit tests. The test was passed successfully:
![compile results](https://i.imgur.com/DHO7ipZ.png)
![test results](https://i.imgur.com/QoXJuzY.png)  


- Created the following task on the build.gradle, in order to create a copy of the src folder into a new backup folder:
```
task doBackup(type: Copy) {
    from 'src'
    into 'backup'
}
```


- Tried to run the command ``gradle doBackup``, and it ran successfully:
![console backup](https://i.imgur.com/80dRcVJ.png)
![backup folder](https://i.imgur.com/Bgp60mH.png)  


  
- Created the following task on the build.gradle, in order to create a zipped copy of the src folder into the distributions folder:
```
task zipSource(type: Zip){
    archiveFileName = "source.zip"
    destinationDirectory = layout.buildDirectory.dir('distributions')

    from 'src'
}
```


- Tried to run the command ``gradle zipSource``, and it ran successfully:
![console zip](https://i.imgur.com/BERgCAj.png)
![zip folder](https://i.imgur.com/bQa0WB5.png)  
