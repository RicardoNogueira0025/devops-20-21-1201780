
# CA1 Work Process

- Created new branch using _git branch email-field_
- Changed to the new branch using _git checkout email-field_
- Created the new branch on the remote server using _git push --set-upstream origin email-field_
- Created a new tag with _git tag 1.2.0_ Checked if the tag was created with _git tag_
- Pushed the new tag into the remote server with _git push origin 1.2.0_
- Implemented the changes on the project. At the end of each small-scale implementation, I staged the changes with _git add ._ and commited them with _git commit -m "message"_
- Merged the changes back into master using _git checkout master_ to go back into the master branch and then _git merge email-field_ to merge the email-field branch into my current branch. There were no conflicts to resolved.
- Created a new tag using _git tag 1.3.0_ and pushed it with _git push origin 1.3.0_
- By mistake, implemented the @ verification during the 1.2.0 version. In order to follow the class assignment, the new branch and merge was created either way, using _git branch fix-invalid-email_, then moved to that branch using _git checkout fix-invalid-email_ and finally created that branch in the remote server with _git push --set-upstream origin fix-invalid-email_
- Created a new tag using _git tag 1.3.1_
- Merged the fix-invalid-email branch with the master branch using _git checkout master_ to change my working branch and _git merge fix-invalid-email_ to merge fix-invalid-email into the master.
- Created the final tag using _git tag ca1_

## Alternative To Git

 Upon researching, I came across a number of alternatives to Git for VCS. I decided upon [Mercurial](https://www.mercurial-scm.org/), using [SourceForge](https://sourceforge.net/) as a compatible source repository.

***Git vs Mercurial***

| Git      | Mercurial |
| ----------- | ----------- |
| Compatible with the majority of repositories      | Some repositories have deprecated support       |
| Takes up more space on the repository   | Each version is lighter on the repository       |
|Saves versions as snapshots|Saves versions as differences|
|Decentralized|Decentralized|
|Mutable history|Immutable history|


Mercurial is immutable. If we commit something, it will be present in the version history forever. There are no tools to manipulate history by default.

Git allows for manipulating history. There's rebase, commit amend, reset and filter-branch commands that allow for rollbacks of our version history and manipulation of the previous stored versions.

In Mercurial there are two types of branches: named branches and repository clones. Cloning a repository is how you branch; When you want to do something different, you must clone your repo somewhere to your local machine. Named branches allow you to have a workflow where you can switch back and forth between branches in the same working directory.

In Mercurial, every changeset belongs to a named branch. That is, the branch name is stored in the changeset. It's possible to have more than one branch with the same name, and this can cause confusion in a team.

In Git, a branch is just a header. This header must be explicitly shared across repositories.


# Alternative Implementation

- After creating my project in the SourceForge repository, I cloned it into my local machine using _hg clone ssh://draxicor@hg.code.sf.net/p/ca1/code ca1-code_

**Repository can be found [here](https://sourceforge.net/p/ca1/code/ci/default/tree/)**


![hgclone](https://i.imgur.com/uzCkZBM.png)

- A default .hg folder (similar to git) and README file were created on the project.

![hgfolder](https://i.imgur.com/yyYFmue.png)

- I transferred the tut-basic project to the created repository folder.

- Used _hg add_ (without the . at the end) to stage the project folder for the initial commit.

- Checked if the add command was successful with _hg status_

- Used _hg commit -m 'Initial commit'_ to commit.

- Pushed the commit to the remote repository using _hg push_

**Comparison: This initial phase is very similar to Git in terms of usability. The main differences were the manner of presentation in the _hg status_ command, and the fact that cloning and pushing required the repository password input. I couldn't find a way to configure the password, similar to the _git config_ command.**

![firstpush](https://i.imgur.com/nAMHEDD.png)

- Used _hg tag 1.2.0_ to create a new tag.

- Used _hg branch_ to check my current branch (It was named default)

- Created the branch "email-field" using _hg branch email-field_

**Comparison: The command _hg branch_ shows only our current branch, and not the complete list of branches. For a complete list, _hg branches_ (plural) must be used.**

![branches](https://i.imgur.com/T0eDObU.png)

- Made the necessary changes to the project code.
- Used _hg add_ to stage changes.
- Used _hg commit -m "Implemented email-field"_ to commit the changes
- Tried to push the changes to the repository with _hg push_ but failed. The remote needs to accept the new branch first. Used _hg push --new-branch_ to push the changes into the new branch.

![newbranchpush](https://i.imgur.com/fzmlLaA.png)

- Used _hg update default_ to switch to the default branch.

- Used _hg merge email-field_ to merge the changes into the default branch.
  
- Used _hg add_ to stage changes.
  
- Used _hg commit -m "Implemented email-field"_ to commit the changes

**Comparison: The process of merging branches is identical to Git**

- Created new tag using _hg tag 1.3.0_
- Created new branch using _hg branch fix-email-field_
- Made the changes to the project
- Added the changes to staging using _hg add_
- Commited the changes using _hg commit -m "added validation to the email format"_
- Pushed the new branch to remote using _hg push --new-branch_
- Changed my working branch to default using _hg update default_ and then merged the branch using _hg merge fix-email-field_
- Created the new tag using _hg tag 1.3.1_
- Created the final tag using _hg tag ca1_
- Used _hg push_ to push the new tags and final version of the code to the remote repository's default branch













# Application

**Analysis**

_Function: Adding an email field._

The input for an email should accept only email-format Strings (ie. _word@domain.extension_). It should reject everything not on this format, as well as null, blank and empty Strings.

**Design**

In order to achieve this function, a Regular Expression will be used to validate the inputted String. We also need to allow for the display of the field on the frontend. 
The validation method will replace an invalid email with an error message. Ideally, it would throw an exception (which would be caught and handled).


*DataBase Loader*
![input](https://i.imgur.com/csXrCLO.png)
*Output*
![output](https://i.imgur.com/LJmdriA.png)

**Implementation**

 - A new private String attribute was added to the Employee class.
- A validation method was created, to be called in the constructor:
```java
  private void checkEmail(String email) {
        if (!isValidEmail(email)) {
            this.email = INVALIDEMAIL;
        } else {
            this.email = email;
        }
    }

  private boolean isValidEmail(String email) {
        if (email == null)
            return false;
        if (email.isEmpty() || email.trim().length() == 0)
            return false;
        String emailRegex = "[A-Z0-9a-z._%-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";

        Pattern pat = Pattern.compile(emailRegex);
        return pat.matcher(email).matches();
    }
  ```
  
 This will check if a String is compliant with a Regex expression for a valid email, as well as check if it's not null, blank nor empty. If this validation fails, it will replace the email with an error message.

In order for the server to be compatible with the new integration, the app.js file was modified to feature an ```<th>Email</th>``` field and a ```<td>{this.props.employee.email}</td>``` field.

Observations: The null/blank/empty validation was done at the same time that the @ validation. 


**Unit Tests**

The following unit tests were built to ensure that the business rules were followed upon the creation of an Employee object:

```java
String INVALIDEMAIL = "Inserted email isn't valid";

    @DisplayName("Assert a valid email is accepted")
    @Test
    void shouldBeSameCreateEmployeeWithValidEmail() {
        String expected = "email@domain.com";
        Employee employee = new Employee("Legolas", "Thranduilion", "Elf", "Fighter", "email@domain.com");
        String result = employee.getEmail();
        assertEquals(expected, result);

    }

    @DisplayName("Assert a null, blank or empty email is replaced with the error message")
    @ParameterizedTest
    @ValueSource(strings = {"   "})
    @NullAndEmptySource
    void shouldBeSameNullBlankEmptyEmailReplacedByError(String value) {
        String expected = INVALIDEMAIL;
        Employee employee = new Employee("Gimli", "son of Gloin", "Dwarf", "Barbarian", value);
        String result = employee.getEmail();
        assertEquals(expected, result);
    }


    @DisplayName("Assert an invalid email is replaced with the error message")
    @ParameterizedTest
    @ValueSource(strings = {"aragorn","aragorn.middleearth.com","aragorn@@rivendel.me","!aragorn@rivendel.me"})
    @NullAndEmptySource
    void shouldBeSameInvalidEmailReplacedByError(String value) {
        String expected = INVALIDEMAIL;
        Employee employee = new Employee("Aragorn", "Telcontar", "Human", "Ranger", value);
        String result = employee.getEmail();
        assertEquals(expected, result);
    }
```
 
