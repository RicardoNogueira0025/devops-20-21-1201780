# CA4

This project demonstrates how to run a container with Tomcat and other with H2.

- After installing Docker Desktop, I copied the provided example docker-compose structure.

 - Analysing the docker-compose.yml file:
![docker-compose](https://i.imgur.com/CcPvjOs.png)
   - The IP addresses and port numbers of both VMs are important to note in order to access them later.
    - The _web_ VM depends on the _db_ VM. Any failure in the creation of the _db_ machine will not enable the creation of _web_
    
- Analysing the Dockerfile on the web folder:
![web dockerfile](https://i.imgur.com/tJ9ZPod.png)
  - I inserted my repository address.
  - Changed the working directory to the correct directory of the project
  - Corrected the war file name to match the build that will be created by my project.
    
- Ran the ```docker-compose build``` command, but there was an error on running the _./gradlew clean build_ command.
![error gradlew](https://i.imgur.com/ProDNVI.png)
  
- Added the following line to the web Dockerfile to allow for execution permission for the gradle wrapper:
![chmod](https://i.imgur.com/4CvkXDz.png)
  
- After running the ```docker-compose build``` command again, the build was successful:
![build](https://i.imgur.com/efcGYfv.png)

- Then, I executed ```docker-compose up``` to initialize the Virtual Machines. 
  
We use ```docker-compose up``` to initialize the images and VMs. But, what is "up"? Does the console have a sense of direction? What dimensions are present in a bash console. Does the computer know where "up" is? Do we? 
   
The word up usually has a positive connotation - _thumbs up, look up, go up in life_ - whereas down usually has a negative connotation - _look down, go down etc._

Why is this so and when did such an usage begin?

This phenomenon not only occurs in many more word-pairs (high/low, tall/short, over/under), it is also common across many languages. Therefore it's a deeper question than English alone, perhaps linguistics or even human psychology are more appropriate fields to explore this.

Positions of power (kings, priests, gods) have usually been placed up high (altars, daises, mountaintops, pulpits). Birds, trees, fresh air, rain, light, all that comes from above.

Meanwhile, down below, you have dirt and muck and filth, worms and insects, trash, darkness. When disaster strikes, it is usually more dangerous to be further down--you'll be crushed if something falls on you. And there's the fear of the unknown: we can see through the air, but not into deep water or into dark caves (before artificial lighting).

So, upwards is what we strive for. Up is where the pyramids point towards, where space - the ultimate frontier in human advancement -  is located. So why should docker-compose go up? Well, I ask you: Don't we strive for our software - the product of our labor, our personal pyramids - to go where humanity's shared consciousness irrefutably says success is located? So the question is not "why should docker-compose go up?" but rather "Why would it go in any other direction"?

Anyway, the project ran successfully:
![up](https://i.imgur.com/pc4yNHO.png)

- Navigated to _localhost:8082/demo-0.0.1-SNAPSHOT/h2-console/_ on my browser and verified the database was functioning correctly:
  ![db](https://i.imgur.com/i4MNHb8.png)
  

- Navigated to _localhost:8080/demo-0.0.1-SNAPSHOT/_ on my browser and verified the frontend was displaying without problem:
![frontend](https://i.imgur.com/W6Iv4Iy.png)
  
- Logged into docker:
![login](https://i.imgur.com/cqs72FV.png)
  
- Used the command ```docker images``` to check the ID of my images:
![dockerimages](https://i.imgur.com/CEdhBwD.png)
  
- And changed the tags of the images:
![tags](https://i.imgur.com/5SlDlU6.png)
  
- Finally, pushed both images using the following commands:

```docker push ricardonogueira/ca4_web:CA4```
  
```docker push ricardonogueira/ca4_db:CA4```

![push](https://i.imgur.com/eUiVn3i.png)

- Confirmed that the push was successful by viewing my repository:
![repo](https://i.imgur.com/cXKcZrb.png)
  
- Inserted the command ```docker exec -it 892eacd64940 cp jpadb.mv.db /usr/src/data``` to copy the DB data
![volume command](https://i.imgur.com/c1VXQwv.png)
  
- Confirmed the data was copied successfully:
![data](https://i.imgur.com/527Lsuq.png)
  
#Alternative (Heroku)


  

  