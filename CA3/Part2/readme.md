# CA3 Part 2

- Downloaded the provided Vagrantfile into the CA3/Part2 folder

![initial vagrantfile](https://i.imgur.com/jycYfhZ.png)

- Changed lines 70, 71 and 75 of the Vagrantfile to clone my repository and match the name of the build file
![Vagrantfile change](https://i.imgur.com/i3RPqXj.png)
  
- In the build.gradle file, added ```id 'war'``` to the plugins section and ```providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat' ``` to the dependencies section.

- Created the ServletInitializer class:
![serverinit class](https://i.imgur.com/oro5t48.png)
  
- Added the following lines to the application.properties files to allow for support for the H2 console:
```
  server.servlet.context-path=/demo-0.0.1-SNAPSHOT
  spring.data.rest.base-path=/api
  #spring.datasource.url=jdbc:h2:mem:jpadb
  spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
  spring.datasource.driverClassName=org.h2.Driver
  spring.datasource.username=sa
  spring.datasource.password=
  spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
  spring.jpa.hibernate.ddl-auto=update
  spring.h2.console.enabled=true
  spring.h2.console.path=/h2-console
  spring.h2.console.settings.web-allow-others=true
  ```
- Altered the application context path to match the project build:
![app context path](https://i.imgur.com/Ay1LR8f.png)
  
- Removed the backslash on line 6 of the main/resources/templates/index.html file

- Pushed the changes into the repository so that it's ready for the Vagrantfile script to clone

- Ran the command ```vagrant up``` on the CA3/Part2 folder. There was an error in initializing the db VM: 
![error](https://i.imgur.com/ymZhW4B.png)
  
- Re-installed Vagrant and the issue was resolved. However, the process failed due to the lack of the repository password:
![no pass repo](https://i.imgur.com/jYAHtr4.png)
  
- Removed private status from the repository and tried again after halting and destroying de VMs. The proccss failed due to the 8080 port being used by another process:
![8080](https://i.imgur.com/XSxyEcV.png)
  
- Found and killed the process on 8080. Checked to see if 8082 and 9092 are free:

![taskkill](https://i.imgur.com/e2Nd6uQ.png)
Alternatively, I could have changed the port numbers on the Vagrantfile

- The build process kept getting stuck on the Install Frontend task. After several attempts to fix it, I re-downloaded the base Vagrantfile and ran it without problem. I again replaced the lines 70, 71 and 75 to match my project, and it finished the build, although the data wasn't present in the frontend:
![](https://i.imgur.com/t5FQ87r.png)
  Even though the data was present in the database:
  ![](https://i.imgur.com/lYizZ86.png)
  
- Changed the java version in the build.gradle file to 1.8 and built the project again. It now works!
![working](https://i.imgur.com/SvtSM7k.png)

#Alternative (Hyper-V)
As a Windows user, I had a VirtualBox alternative available on my system by default: Hyper-V.

Hyper-V is a type 1 hypervisor, meaning it runs directly on the computer hardware. When the host machine starts, HyperV also starts from the BIOS. Meaning that if HyperV is enabled, it is always on if the host machine is on.
VirtualBox, on the other hand, is a type 2 hypervisor. It runs over the host machine's Operating System. It can be started and closed by the user without the need to restart the host machine.

![types](https://i.imgur.com/0xsxKr7.png)

- Since VirtualBox is the default provider for Vagrant, I need to first activate HyperV on my host machine:

![hyper-v install](https://i.imgur.com/rtehmIw.png)

- Then, I  ran Vagrant specifying the provider, using the following command:
```vagrant up --provider=hyperv```
![specify provider](https://i.imgur.com/nqQxyxZ.png)

- However, as shown in the above image, the box isn't compatible with Hyper-V. I need to find an alternate box. I visited [the vagrantup box list page](https://app.vagrantup.com/boxes/search?provider=hyperv) to search for HyperV compatible boxes. I came upon [bento/ubuntu-16.04](https://app.vagrantup.com/bento/boxes/ubuntu-16.04).

- I changed the Vagrantfile to use Bento Ubuntu box and disable the use of shared folders:
![bento](https://i.imgur.com/42XS4la.png)
  
- After that change, the vagrant up was successful and the VMs were running on Hyper-V:
![v-up bento](https://i.imgur.com/4IUitLc.png)
![hyperv running](https://i.imgur.com/hh5nD1l.png)
  
  - Changed the application.properties file to retrieve the database info from the correct VM IP:
![](https://i.imgur.com/zHzTyWH.png)

![](https://i.imgur.com/L5zE7mR.png)

- Using the correct web VM IP, I was able to access the project:
![web ip](https://i.imgur.com/ppdVcHc.png)
 ![frontend](https://i.imgur.com/wub3QWp.png) 
  
# Analysis

HyperV always attributes a new IP address to the virtual machines upon each startup. This means that the IP address to access the frontend, as well as the IP address of the database registered in the application.properties, must be changed each time we start the VMs.
With VirtualBox, this is not the case. Due to this, VirtualBox is more reliable for distribution
  





  
  
