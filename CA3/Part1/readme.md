# CA3 Part 1

##Walkthorugh

- Created a Virtual Machine with Oracle VM Virtual Box using the steps explained in the class. Ubuntu was installed, and a new Host network was set up.

- The required dependencies were installed using the commands:
```aidl
sudo apt install git
sudo apt install openjdk-8-jdk-headless
sudo apt install maven
sudo apt install gradle
```
 - Cloned my individual repository into the VM
![repository in VM](https://i.imgur.com/h9EIF9w.png)
   
- Accessed the tut-basic project from CA1 by running the server on the vm and using my host's browser
![tut-basic-running](https://i.imgur.com/QLCPNiI.png)

- Tried running the chat application server using ```gradle runServer``` but got the following error:
![firsterror](https://i.imgur.com/UmHj9mS.png)
  
- Enabled execution permissions os gradlew using ```chmod +x gradlew```

- Removed the *.jar setting on the project gitignore in order to have the jar files of the gradle wrapper on the VM

- The server now runs on the VM successfully:
![server running on VM](https://i.imgur.com/CCEyQgy.png)
  
- Modified the argument of the runClient task on the host machine project to accept the IP of the Virtual Machine:
![ip argument](https://i.imgur.com/11MdZYX.png)
  
- Executed the runClient task twice on my Host machine (left on the image) and the new users was detected on the server running on the VM (right on the image)
![chat server running and client](https://i.imgur.com/pRWb11A.png)
  
## Why run the Server on the VM and the client on the Host?
The ubuntu VM does not have a UI, so the client would not be able to be executed there.
Additionally, in order to simulate more accurately how a chat system would work in real conditions, the Chat server will always be running on a remote machine, and not on the client's machine. By instructing our host machine's client to access the Virtual Machine's client, we are effectively simulating accessing a server on another computer.

  
