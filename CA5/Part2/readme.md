#CA5 Part 2

- Added the Docker related plugins to Jenkins:
  ![plugins](https://i.imgur.com/5LdSIx4.png)

- Added the example stage to my Jenkinsfile:
  ![jenkinsfileupdate](https://i.imgur.com/GSHodeM.png)

- Ran the build. The Docker Image task failed:
  ![nodocker](https://i.imgur.com/tOqnMZV.png)

- Shifted focus to Javadoc. Installed the Javadoc and Html Publisher plugins on Jenkins.

-Added the Javadoc stage to the Jenksinsfile:
![javadocstage](https://i.imgur.com/XE2Nii9.png)

- Ran the build. Javadoc task executed successfully:
  ![javadocyes](https://i.imgur.com/phbf07F.png)

- And published in Jenkins:
  ![javadocpublished](https://i.imgur.com/Uao1HEh.png)

- Focusing back on the Docker Image, I was missing the _Docker Pipeline_ plugin. Installed it:
  ![missing plugin](https://i.imgur.com/fD8jDlZ.png)

- This time, the error was different:
  ![second docker error](https://i.imgur.com/8R93IKY.png)

- Changed the Jenkinsfile syntax:
  ![jenkinsfile syntax](https://i.imgur.com/W0x6aZe.png)

- Created a dockerfile:
  ![dockerfile](https://i.imgur.com/mOGPALm.png)

- This time, the Jenkins build was successful:
  ![buildyes](https://i.imgur.com/ZpWE5kD.png)


- Altered the stage to allow for publishing:
  ![publish](https://i.imgur.com/8E2f6t6.png)

- The build was published in my DockerHub:
  ![dockerpublish](https://i.imgur.com/xh2ySxQ.png)
  
#Alternative (Buddy)

## Alternative Analysis

Buddy is a CD/CI tool that's readily available online (without the need to download anything). 

It is not open-source (unlike Jenkins), and therefore does not have the extensive user-based plugin library that Jenkins has to offer.

-However, Buddy is a lot simpler to set up a new pipeline and build our project.
Adding a new pipeline is similar to Jenkins, with a name and basic trigger options:
![newpipelinebuddy](https://i.imgur.com/vo6iCsP.png)

-Stages of the Pipeline are called actions, and are pre-setup on the UI:
![actionsex](https://i.imgur.com/brETtJl.png)

-Buddy also allows for direct browsing of the linked repository:
![repobuddy](https://i.imgur.com/ZBJEGED.png)

In conclusion, Buddy is much more user friendly than Jenkins, requiring less engineering/Devops knowledge to setup. It's also a lot faster to add a new pipeline and debug problems, as it allows to start a failed build from the point of failure after editing the action that was incorrect/correcting the project.
There's also the added bonus of not needing to download or be dependent upon anything in our local machine.

## Alternative Implementation

- Logged into Buddy with my atlassian account and connected the Devops repo
![repoconnect](https://i.imgur.com/bEPSBuh.png)
  
- The repo was having trouble connecting, so I cloned the repo into my GitHub account and used that to link to Buddy.
![git](https://i.imgur.com/yVMKTmC.png)
  
- Added the Actions to the pipeline:
![actions](https://i.imgur.com/Z45F6qa.png)
  
- Each action was aided with the Buddy User interface. Gradle tasks can be selected with the gradle version of our choice:
![gradletaskbuddy](https://i.imgur.com/s7brs0K.png)
  
- The same with Docker, aided with a UI:
![dockerbuddy](https://i.imgur.com/H4q0ouP.png)
  ![dickerbuddy2](https://i.imgur.com/roIkCJ3.png)
  
-Upon running the pipeline, it was successful:
![runbuddy](https://i.imgur.com/mdl3l39.png)

![pipleine](https://i.imgur.com/SuzJfUP.png)






