#CA5 Part 1

- Downloaded the Jenkins war file and ran the command ```java -jar jenkins.war```

- Accessed Jenkins using the 8080 port:
  ![jenkins](https://i.imgur.com/ru9IEij.png)

- Created a new Pipeline with a SCM Script:
  ![config](https://i.imgur.com/b6GDWaB.png)

- Created the Jenkinsfile, replacing the Build with an Assemble stage and adding the Test stage:
  ![jenkinsfile](https://i.imgur.com/j6DGB42.png)

- Committed all the changes and pushed into the repository.

- Started a build manually on Jenkins, but it failed since the path to the Jenkinsfile was wrong.
  ![fail](https://i.imgur.com/Zr3tS8h.png)

- Corrected the Jenkinsfile path and ran the build again. It failed, but this time it reached the Jenkinsfile
  ![fail2](https://i.imgur.com/eqLCCyl.png)

- The console output indicates that it can't find gradlew
  ![cantfindgradle](https://i.imgur.com/DHmn43P.png)

- Changed the Jenkinsfile to reflect a directory change to the place where gradlew is, and changed the command type to bat:
  ![fix](https://i.imgur.com/qZNbz2U.png)

- After committing and building, it was successful:
  ![Success](https://i.imgur.com/kWJYJ9S.png)